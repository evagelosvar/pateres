#!/bin/sh
echo "Content-type: text/html"
echo ""



#folder=ttt/

#folder=/builds/evagelosvar/pateres
folder=$1
basename $folder

##################
linux=`echo $SHELL`
echo $linux
if [ "$linux" = "/bin/zsh" ]
then
alias pandoc=./bin/pandoc.exe

folder=`echo "$folder" | sed 's@^\/@@g'`

fi
##########################
 
 
#######################################
#Convert docx files to md files
#######################################
for docfile in `find "$folder" -type f -name '*.docx'`
do

echo "$docfile"
IFS='
'
fileDir=`dirname "$docfile"`
fileNameNoExt=`basename $docfile .docx`

num=`shuf -i 1-1000 -n 1`
 
#./bin/pandoc.exe --extract-media $folder"/""docs""/""img/"$num  --mathjax -s -f docx -t markdown-simple_tables-multiline_tables-grid_tables -o "$fileDir""/""$fileNameNoExt"".md" $docfile
 
pandoc --extract-media $folder"/""docs""/""img/"$num  --mathjax -s -f docx -t markdown-simple_tables-multiline_tables-grid_tables -o "$fileDir""/""$fileNameNoExt"".md" $docfile
 

 
cat "$fileDir""/""$fileNameNoExt"".md" | sed 's@'$folder"/""docs""/"'@'"/"'@g' > "$fileDir""/"temp.md

mv "$fileDir""/"temp.md "$fileDir""/""$fileNameNoExt"".md"

done
 
