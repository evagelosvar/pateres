#!/bin/sh
echo "Content-type: text/html"
echo ""


folder=/builds/evagelosvar/pateres/
mkdir $folder"/""temp"

#######################################
#Convert doc files to docx files
#######################################

for docfile in `find "$folder" -type f -name '*.doc'`
do

############################################
#Copy the doc file to directory (temp) without greek chars
############################################
 
tempdocFile=$folder"/""temp""/""tempdocFile.doc"
cp $docfile $folder"/""temp""/""tempdocFile.doc"


echo "$docfile"
 
outputdir=$folder"/""temp"
 
 
soffice --headless --convert-to docx --outdir "$outputdir" $tempdocFile 
 

cp $folder"/""temp""/""tempdocFile.docx" `dirname "$docfile"`"/"`basename $docfile .doc`".docx"

done

#######################################
#Convert docx files to md files
#######################################
for docfile in `find "$folder" -type f -name '*.docx'`
do


tempdocFile=$folder"/""temp""/""tempdocFile.docx"
cp $docfile $folder"/""temp""/""tempdocFile.docx"

echo "$docfile"

outputdir=$folder"/""temp"

pandoc --extract-media /pateres/img2 -s -f docx -t markdown_mmd -o `dirname "$docfile"`"/"`basename $docfile .docx`".md" $tempdocFile

done

#######################################
#Create md files from pdf files, having a link to the corresponding pdf file
#######################################
 
for file in `find "$folder" -type f -name '*.pdf'`
do 
IFS='
'
mdfile=`echo  "$file"| sed 's/\.pdf/\.md/g'`
dirname $mdfile

getFilenameNoext=`basename $mdfile .md`

echo '![](logo.jpg)' > $mdfile
echo '<br>' >> $mdfile
echo "[click me!](""$getFilenameNoext"".pdf)" >> $mdfile

#pdftotext $file $file".txt" 
#soffice --headless --convert-to docx:"MS Word 2007 XML" --outdir "$outputdir" $file

#soffice --headless --infilter="writer_pdf_import" --convert-to doc $file
#soffice --infilter="writer_pdf_import" --outdir "$outputdir" --convert-to docx $file
done

